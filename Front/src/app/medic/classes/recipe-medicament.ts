import {Medicament} from "./medicament";

export class RecipeMedicament {
  medicament?: Medicament
  count: number = 0
}
