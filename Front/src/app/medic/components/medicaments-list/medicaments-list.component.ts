import {Component, Input, OnInit, Output} from '@angular/core';
import {MedicService} from "../../services/medic.service";
import {Medicament} from "../../classes/medicament";
import {RecipeMedicament} from "../../classes/recipe-medicament";

@Component({
  selector: 'app-medicaments-list',
  templateUrl: './medicaments-list.component.html',
  styleUrls: ['./medicaments-list.component.css']
})
export class MedicamentsListComponent implements OnInit {

  @Input() readonly?: boolean

  required: boolean = false
  mediaments?: Medicament[]
  recipeMedicaments: RecipeMedicament[] = [{medicament: undefined,
                                            count: 0}]
  constructor(
    private service: MedicService,
  ) {
  }

  selectMedicament(
    recipeMedicament: RecipeMedicament,
    medicamentEv: Event): void {
	
	let medicament = (medicamentEv.target as HTMLInputElement).value
		console.log(recipeMedicament)
		console.log(medicament)
    if(recipeMedicament.medicament !== undefined) recipeMedicament.medicament.selected = false
	let med = undefined
	if (this.mediaments === undefined) return
	for (let i of this.mediaments)
	{
		if (Number(medicament) === i.id)
		{
			med = i
			break
		}
		
	}
	if (med === undefined) return
    recipeMedicament.medicament = med
    med.selected = true;
    this.validate()
  }

  validate(): void {
	  console.log(this.recipeMedicaments)
    for(let element of this.recipeMedicaments) {
		console.log(element.medicament)
		console.log(element.count)
      if (element.medicament === undefined || element.count <= 0) {
        this.required = false
        return
      }
    }
    this.required = true
  }

  addRecipeMedicament() {
    this.recipeMedicaments.push({medicament: undefined,
                                count: 0})
    this.validate()
  }

  deleteMedicament(recipeMedicament: RecipeMedicament) {
    if(recipeMedicament.medicament !== undefined)
    {
      recipeMedicament.medicament.selected = false
    }
    recipeMedicament.medicament = undefined
    this.recipeMedicaments.splice(this.recipeMedicaments.indexOf(recipeMedicament), 1)
    this.validate()
  }
  ngOnInit() {
    this.service.getMedicaments("/apothecary/getMedicaments").subscribe(result =>
    {
      this.mediaments = result;
    },
    error =>
    {
      console.log(error)
    })
  }

  buildRecipe(): {id: number, count: number}[] {
    let medicaments: {id: number, count: number}[] = []

    for(let element of this.recipeMedicaments) {
      if(element.medicament !== undefined)
      {
        medicaments.push({id: element.medicament.id, count: element.count})
      }
    }
    return medicaments
  }
}
