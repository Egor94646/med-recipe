import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {AuthModule} from "./auth/auth.module";
import {AppRoutingModule} from "./app-routing.module";
import {FormsModule} from "@angular/forms";
import {MedicModule} from "./medic/medic.module";
import {ApothecaryModule} from "./apothecary/apothecary.module";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import { NgxPrintModule } from 'ngx-print';

@NgModule({
  declarations: [
    AppComponent
  ],
    imports: [
        BrowserModule,
        AuthModule,
        AppRoutingModule,
        FormsModule,
        MedicModule,
        ApothecaryModule,
        HttpClientModule,
        NgxPrintModule,
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
