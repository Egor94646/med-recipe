export class Recipe {
  namePat: string = ""
  sNamePat: string = ""
  tNamePat: string = ""
  agePat: string = ""
  validityPeriod: string = ""

  static parse(obj: any): Recipe|undefined {
    if (obj === undefined || obj === null) return undefined

    const recipe = new Recipe()
    Object.assign(recipe, obj)

    return recipe
  }
}
