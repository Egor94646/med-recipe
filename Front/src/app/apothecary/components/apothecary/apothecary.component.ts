import {Component, ViewChild} from '@angular/core';
import {Router} from "@angular/router";
import {ApothecaryService} from "../../services/apothecary.service";
import {Medicament} from "../../classes/medicament";
import {Recipe} from "../../classes/recipe";

@Component({
  selector: 'app-apothecary',
  templateUrl: './apothecary.component.html',
  styleUrls: ['./apothecary.component.css']
})
export class ApothecaryComponent {

  @ViewChild("code") codeInput: any;
  constructor(
    private router: Router,
    private service: ApothecaryService,
  ) {
  }

  goToRecipe(code: string) {
    this.service.checkRecipe("/apothecary/checkRecipeExist/" + code).subscribe(
      result => {
        if(result) {
          this.router.navigate(['apothecary/recipe', code])
        }
      }
    )
  }
  exit(): void {
    this.router.navigate([''])
  }
}
