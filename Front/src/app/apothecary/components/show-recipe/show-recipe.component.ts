import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription} from "rxjs";
import {ApothecaryService} from "../../services/apothecary.service";
import {Recipe} from "../../classes/recipe";
import {Medicament} from "../../classes/medicament";

@Component({
  selector: 'app-show-recipe',
  templateUrl: './show-recipe.component.html',
  styleUrls: ['./show-recipe.component.css']
})
export class ShowRecipeComponent implements OnInit{

  private paramsSubscription?: Subscription
  recipeId: number = 0
  status: "fullReleased"|"notValidity"|"valid"|"notExist"|"" = ""
  recipe?: Recipe
  medicaments?: Medicament[]
  valid: boolean = false

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: ApothecaryService,
  ) {
  }
  ngOnInit() {
    this.paramsSubscription = this.route.params.subscribe(({id}) => {
      this.recipeId = id
      this.service.getRecipe("/apothecary/getRecipe/" + this.recipeId).subscribe(result => {
        this.status = result.status
        this.recipe = result.data.recipe
        this.medicaments = result.data.medicaments
      })
    })
  }

  checkOut() {
    if(this.medicaments === undefined) return
    const ret: {recipeId: number, debitMedicaments: {id: number, count: number}[]} = {
      recipeId: this.recipeId,
      debitMedicaments: []
    }

    for(const medicament of this.medicaments) {
      if (medicament.debitedCount !== 0) {
        ret.debitMedicaments.push({
          id: medicament.medicamentId,
          count: medicament.releasedCount + medicament.debitedCount
        })
      }
    }

    this.service.checkOut("/apothecary/checkOut", ret).subscribe(result => {
      window.location.reload()
    },
    error => {
      window.location.reload()
    })
  }

  validate() {
    if(this.medicaments === undefined) return
    for(const medicament of this.medicaments) {
      if(
        (medicament.debitedCount < 0) ||
        (medicament.debitedCount > medicament.remainsCount) ||
        (medicament.debitedCount === null)
      ) {
        this.valid = false;
        return
      }
      if(medicament.debitedCount > 0) {
        this.valid = true
      }
    }
  }
  back(): void {
    this.router.navigate(['apothecary'])
  }

}
