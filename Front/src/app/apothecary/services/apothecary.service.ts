import { Injectable } from '@angular/core';
import {RestService} from "../../core/services/rest.service";
import {map, Observable} from "rxjs";
import {Recipe} from "../classes/recipe";
import {Medicament} from "../classes/medicament";

@Injectable({
  providedIn: 'root'
})
export class ApothecaryService {

  constructor(
    private rest: RestService,
    ) { }

  checkRecipe(url: string): Observable<boolean> {
    return this.rest.get(
      url,
    ).pipe(
      map((value: any) => value.status === "exist")
    )
  }

  checkOut(url: string, body: any): Observable<boolean> {
    return this.rest.post(
      url,
      body
    ).pipe(
      map((value: any) => value.status)
    )
  }

  getRecipe(url: string): Observable<{
    status: "fullReleased"|"notValidity"|"valid"|"notExist",
    data: {
      recipe: Recipe|undefined,
      medicaments: Medicament[]|undefined
    }
  }> {
    return this.rest.get(
      url,
    ).pipe(
      map((value: any) => {
        switch (value.status) {
          case "notExist": {
            return {
              status: value.status,
              data: {
                recipe: undefined,
                medicaments: undefined
              }
            }
          }
        }
        const recipe: Recipe|undefined = Recipe.parse(value.recipeInfo)
        const medicaments: Medicament[] = value.recipeInfo.medicaments
          .map((obj: any) => Medicament.parse(obj))
          .filter((obj?: Medicament) => obj !== undefined)
        return {
          status: value.status,
          data: {
            recipe: recipe,
            medicaments: medicaments
          }
        }
      })
    )
  }
}
