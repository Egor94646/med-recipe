import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthComponent} from "./auth/components/auth/auth.component";
import {MainPageComponent} from "./medic/components/main-page/main-page.component";
import {CreateRecipeComponent} from "./medic/components/create-recipe/create-recipe.component";
import {ApothecaryComponent} from "./apothecary/components/apothecary/apothecary.component";
import {ShowRecipeComponent} from "./apothecary/components/show-recipe/show-recipe.component";


const routes: Routes = [
  { path: '', component: AuthComponent },
  { path: 'medic',
    children: [
      { path: '', component: MainPageComponent},
      { path: 'createRecipe', component: CreateRecipeComponent},
    ]
  },
  { path: 'apothecary',
    children: [
      { path: '', component: ApothecaryComponent },
      { path: 'recipe/:id', component: ShowRecipeComponent },
    ]
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
