export const environment = {
  production: true,
  restUrl: window.location.origin + '/api',
  authProvider: window.location.origin + '/auth',
};
